#!/usr/bin/python
import cgi, cgitb, os, sys

UPLOAD_DIR = './cgi/dataset'
print 'Content-Type: text/html; charset=UTF-8'
print
print '''<!DOCTYPE html>
<html>
	<head>
		<title> Big Mart Sales Prediction </title>
	</head>
	<body>
		<form action="cgi/try.py" method="POST" enctype="multipart/form-data">
			 Upload Dataset : <input type="file" name="dataset"></br>
			<input type="submit" value="upload File">
		</form>
		<form action="cgi/modal.py" method="POST">
			Select training dataset : <select name="trainFile">
				<option value="-1"></option>'''
for f in os.listdir(UPLOAD_DIR):
	print "		<option value='" + f +"'>" + f + "</option>"
print '''	</select></br>
			Select testing dataset : <select name="testFile">
				<option value="-1"></option>'''
for f in os.listdir(UPLOAD_DIR):
	print "		<option value='" + f +"'>" + f + "</option>"
print '''
			</select></br>
			MRP_MIn : <input type="text" name="mrp_min"></br>
			MRP_MAX : <input type="text" name="mrp_max"></br>
			<input type="submit" value="use dataset">
		</form>
	</body>
</html>'''
