#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')
import cgi, cgitb, os, sys
import pandas as pd
import numpy as np
import mpld3
from matplotlib import pyplot as plt
from itertools import product
import csv

from scipy.stats import mode
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import train_test_split
from sklearn import cross_validation as model_selection
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn import cross_validation, metrics

def modify_test_data(testFileName, minMrp, maxMrp):
	rangeMRP = np.linspace(minMrp, maxMrp, num = 100)
	df = pd.read_csv("dataset/" + testFileName)
	var1 = df.head(1)
	for val in rangeMRP:
		var1['Item_MRP'] = val
		df = df.append(var1)
	df.to_csv("test_dataset.csv", index = False)
	return "test_dataset.csv"

def num_missing(x):
    return sum(x.isnull())

def impute_missing_values(trainFileName, testFileName):
    train = pd.read_csv('dataset/' + trainFileName)
    test = pd.read_csv(testFileName)

    train['source'] = 'train'
    test['source'] = 'test'
    data = pd.concat([train, test], ignore_index = True)

    #print data.apply(num_missing, axis = 0)

    data['Item_Weight'].fillna(mode(data['Item_Weight']).mode[0], inplace = True)
    data['Outlet_Size'].fillna(mode(data['Outlet_Size']).mode[0], inplace = True)

    data.to_csv("afterimputing.csv", index = False)
    #print data.apply(num_missing, axis = 0)

def modify_item_visibility():
    data = pd.read_csv("afterimputing.csv")

    #print data.apply(num_missing, axis = 0)
    data['Item_Weight'].fillna(mode(data['Item_Weight']).mode[0], inplace = True)
    data['Outlet_Size'].fillna(mode(data['Outlet_Size']).mode[0], inplace = True)
    #print data.apply(num_missing, axis = 0)

    #Determine average visibility of a product
    visibility_avg = data.pivot_table(values='Item_Visibility', index='Item_Identifier')

    ##print visibility_avg
    #Impute 0 values with mean visibility of that product:
    miss_bool = (data['Item_Visibility'] == 0)
    #print 'Number of 0 values initially: %d'%sum(miss_bool)

    #data.loc[miss_bool,'Item_Visibility'] = data.loc[miss_bool,'Item_Identifier'].apply(lambda x : visibility_avg[x])

    data.loc[miss_bool, 'Item_Visibility'] =  data.loc[:,'Item_Visibility'].mean()
    #print 'Number of 0 values after modification: %d'%sum(data['Item_Visibility'] == 0)

    #Determine another variable with means ratio
    data['Item_Visibility_MeanRatio'] = data.apply(lambda x: x['Item_Visibility']/visibility_avg.mean(), axis=1)
    #print data['Item_Visibility_MeanRatio'].describe()

    #Get the first two characters of ID:
    data['Item_Type_Combined'] = data['Item_Identifier'].apply(lambda x: x[0:2])
    #Rename them to more intuitive categories:
    data['Item_Type_Combined'] = data['Item_Type_Combined'].map({'FD':'Food', 'NC':'Non-Consumable', 'DR':'Drinks'})
    #print data['Item_Type_Combined'].value_counts()

    #Years:
    data['Outlet_Years'] = 2018 - data['Outlet_Establishment_Year']
    #print data['Outlet_Years'].describe(), "\n\n\n"

    #Change categories of low fat:
    #print 'Original Categories:'
    #print data['Item_Fat_Content'].value_counts()

    #print '\nModified Categories:'
    data['Item_Fat_Content'] = data['Item_Fat_Content'].replace({'LF':'Low Fat', 'reg':'Regular', 'low fat':'Low Fat'})
    #print data['Item_Fat_Content'].value_counts(), "\n\n"

    #Mark non-consumables as separate category in low_fat:
    data.loc[data['Item_Type_Combined']=="Non-Consumable",'Item_Fat_Content'] = "Non-Edible"
    #print data['Item_Fat_Content'].value_counts()

    #Import library sklearn at top:
    le = LabelEncoder()
    #New variable for outlet
    data['Outlet'] = le.fit_transform(data['Outlet_Identifier'])
    var_mod = ['Item_Fat_Content','Outlet_Location_Type','Outlet_Size','Item_Type_Combined','Outlet_Type','Outlet']
    le = LabelEncoder()
    for i in var_mod:
        data[i] = le.fit_transform(data[i])

    #One Hot Coding:
    data = pd.get_dummies(data, columns=['Item_Fat_Content','Outlet_Location_Type','Outlet_Size','Outlet_Type', 'Item_Type_Combined','Outlet'])
    #print data.dtypes

    #Drop the columns which have been converted to different types:
    data.drop(['Item_Type','Outlet_Establishment_Year'],axis=1,inplace=True)

    #Divide into test and train:
    train = data.loc[data['source']=="train"]
    test = data.loc[data['source']=="test"]

    #Drop unnecessary columns:
    test.drop(['Item_Outlet_Sales','source'],axis=1,inplace=True)
    train.drop(['source'],axis=1,inplace=True)

    #Export files as modified versions:
    train.to_csv("train_modified.csv",index=False)
    test.to_csv("test_modified.csv",index=False)

    #Mean based:
    mean_sales = train['Item_Outlet_Sales'].mean()

    #Define a dataframe with IDs for submission:
    base1 = test[['Item_Identifier','Outlet_Identifier']]
    base1['Item_Outlet_Sales'] = mean_sales

    #Export submission file
    base1.to_csv("alg0.csv",index=False)
    
def modelfit(alg, dtrain, dtest, predictors, target, IDcol, filename):
    #Fit the algorithm on the data
    alg.fit(dtrain[predictors], dtrain[target])
        
    #Predict training set:
    dtrain_predictions = alg.predict(dtrain[predictors])

    #Perform cross-validation:
    cv_score = cross_validation.cross_val_score(alg, dtrain[predictors], dtrain[target], cv=20, scoring='mean_squared_error')
    cv_score = np.sqrt(np.abs(cv_score))
    
    #Print model report:
    #print "\nModel Report </br>"
    #print "RMSE : %.4g" % np.sqrt(metrics.mean_squared_error(dtrain[target].values, dtrain_predictions))
    #print "</br>"
    #print "CV Score : Mean - %.4g | Std - %.4g | Min - %.4g | Max - %.4g" % (np.mean(cv_score),np.std(cv_score),np.min(cv_score),np.max(cv_score))
    #print "</br>"
    
    #Predict on testing data:
    dtest[target] = alg.predict(dtest[predictors])

    #Export submission file:
    IDcol.append(target)
    submission = pd.DataFrame({ x: dtest[x] for x in IDcol})
    submission.to_csv(filename, index=False)
    
    dtrain.head()
    x = dtrain['Item_MRP'].values[:,np.newaxis]
    #y = dtrain['Item_Outlet_Sales'].values
    y = dtrain_predictions
    #fp = open("train.txt", "w")
    #fp.write(y)
    #fp.close()
    plt.plot(x, y, color="g")
    html = mpld3.fig_to_html(plt.gcf())
    print html

print 'Content-Type: text/html; charset=UTF-8'
print
form = cgi.FieldStorage()
if form.has_key('trainFile'):
    train_file = form.getvalue('trainFile')
else:
	print "select training dataset"
if form.has_key('testFile'):
    test_file = form.getvalue('testFile')
else:
	print "select testing dataset"
if form.has_key('mrp_min'):
    mrp_min = form.getvalue('mrp_min')
else:
	print "select min MRP"
if form.has_key('mrp_max'):
    mrp_max = form.getvalue('mrp_max')
else:
	print "select max MRP"

test_file = modify_test_data(test_file, float(mrp_min), float(mrp_max))
impute_missing_values(train_file, test_file)
modify_item_visibility()
train = pd.read_csv("train_modified.csv")
test = pd.read_csv("test_modified.csv")

target = 'Item_Outlet_Sales'
IDcol = ['Item_Identifier','Outlet_Identifier']

predictors = [x for x in train.columns if x not in [target]+IDcol]
# print predictors
alg1 = LinearRegression(normalize=True)
modelfit(alg1, train, test, predictors, target, IDcol, 'alg1.csv')
