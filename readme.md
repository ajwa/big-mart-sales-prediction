SE Project

Given sales data for 1559 products across 10 stores of the Big Mart chain in various cities the task is to build a model to predict sales for each particular product in different stores.


Steps in making the Big mart Sales Prediction Software:

1) Hypothesis generation : Brainstorming about the problem statement
2) Data Exploration : Making inferences about data , feature engineering (combining data:Merge DataFrames)
3) Data Cleaning : Missing Value (fillna method : find mode and fills NULL value:imputing: multi-indexing), 
				   Remove Outliers (visibility)
4) Feature Engineering (Main step:on combined data): Modifying the variables (
					LabelEncoder module : preprocessing module of sklearn,
					range of year estb. converted to integer value,
					creating broad category:LF,lf,low fat; 
					numerical and one-hot coding of categorical variable/creating dummy columns:11->31 variables)
5) Exporting Data : Splitting the combined data into training and testing and passing data to model building
6) Model Building : making Predictive sales model ( Regression algorithm ) on the data
7) Graph Generation (matplotlib:pyplot module) 
8) Price Recommendation Tool
